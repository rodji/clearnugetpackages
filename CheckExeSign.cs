﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanClean
{
    using System.Reflection;
    using System.Runtime.InteropServices;

    public class CheckExeSign
    {
        [DllImport( "mscoree.dll", CharSet = CharSet.Unicode )]
        private static extern bool StrongNameSignatureVerificationEx(
            string wszFilePath, bool fForceVerification, ref bool pfWasVerified );

        /// <summary>
        /// Not sure that it works good
        /// better solution? : http://www.pinvoke.net/default.aspx/wintrust.winverifytrust
        /// </summary>
        /// <param name="pathToExe"></param>
        public static void RequireRightSingature( string pathToExe )
        {
            bool pfWasVerified = false;
            if ( !StrongNameSignatureVerificationEx( pathToExe, true, ref pfWasVerified ) ) {
                throw new Exception("Untrusted file");
            }
        }
    }
}
