﻿namespace CleanClean
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) ) {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxPendingList = new System.Windows.Forms.TextBox();
            this.buttonGetNuGetCache = new System.Windows.Forms.Button();
            this.textBoxManualList = new System.Windows.Forms.TextBox();
            this.buttonAddNugetCache = new System.Windows.Forms.Button();
            this.buttonDeleteFiles = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonClearLog = new System.Windows.Forms.Button();
            this.buttonCopyDown = new System.Windows.Forms.Button();
            this.buttonSaveManualList = new System.Windows.Forms.Button();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.buttonClearPendingList = new System.Windows.Forms.Button();
            this.buttonCheckPendingList = new System.Windows.Forms.Button();
            this.labelManualList = new System.Windows.Forms.Label();
            this.labelPendingList = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxPendingList
            // 
            this.textBoxPendingList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPendingList.Location = new System.Drawing.Point(34, 189);
            this.textBoxPendingList.Multiline = true;
            this.textBoxPendingList.Name = "textBoxPendingList";
            this.textBoxPendingList.Size = new System.Drawing.Size(613, 193);
            this.textBoxPendingList.TabIndex = 0;
            // 
            // buttonGetNuGetCache
            // 
            this.buttonGetNuGetCache.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGetNuGetCache.Location = new System.Drawing.Point(34, 575);
            this.buttonGetNuGetCache.Name = "buttonGetNuGetCache";
            this.buttonGetNuGetCache.Size = new System.Drawing.Size(118, 23);
            this.buttonGetNuGetCache.TabIndex = 1;
            this.buttonGetNuGetCache.Text = "Get NuGet cache";
            this.buttonGetNuGetCache.UseVisualStyleBackColor = true;
            this.buttonGetNuGetCache.Click += new System.EventHandler(this.buttonGetNuGetCache_Click);
            // 
            // textBoxManualList
            // 
            this.textBoxManualList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxManualList.Location = new System.Drawing.Point(34, 12);
            this.textBoxManualList.Multiline = true;
            this.textBoxManualList.Name = "textBoxManualList";
            this.textBoxManualList.Size = new System.Drawing.Size(613, 142);
            this.textBoxManualList.TabIndex = 2;
            // 
            // buttonAddNugetCache
            // 
            this.buttonAddNugetCache.Location = new System.Drawing.Point(34, 388);
            this.buttonAddNugetCache.Name = "buttonAddNugetCache";
            this.buttonAddNugetCache.Size = new System.Drawing.Size(118, 23);
            this.buttonAddNugetCache.TabIndex = 3;
            this.buttonAddNugetCache.Text = "Add NuGet cache";
            this.toolTip1.SetToolTip(this.buttonAddNugetCache, "Deletes to Recycle Bin");
            this.buttonAddNugetCache.UseVisualStyleBackColor = true;
            this.buttonAddNugetCache.Click += new System.EventHandler(this.buttonAddNugetCache_Click);
            // 
            // buttonDeleteFiles
            // 
            this.buttonDeleteFiles.Location = new System.Drawing.Point(406, 388);
            this.buttonDeleteFiles.Name = "buttonDeleteFiles";
            this.buttonDeleteFiles.Size = new System.Drawing.Size(118, 23);
            this.buttonDeleteFiles.TabIndex = 4;
            this.buttonDeleteFiles.Text = "Remove files";
            this.toolTip1.SetToolTip(this.buttonDeleteFiles, "Deletes to Recycle Bin");
            this.buttonDeleteFiles.UseVisualStyleBackColor = true;
            this.buttonDeleteFiles.Click += new System.EventHandler(this.buttonDeleteFiles_Click);
            // 
            // buttonClearLog
            // 
            this.buttonClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonClearLog.Location = new System.Drawing.Point(158, 575);
            this.buttonClearLog.Name = "buttonClearLog";
            this.buttonClearLog.Size = new System.Drawing.Size(118, 23);
            this.buttonClearLog.TabIndex = 6;
            this.buttonClearLog.Text = "Clear";
            this.toolTip1.SetToolTip(this.buttonClearLog, "Deletes to Recycle Bin");
            this.buttonClearLog.UseVisualStyleBackColor = true;
            this.buttonClearLog.Click += new System.EventHandler(this.buttonClearLog_Click);
            // 
            // buttonCopyDown
            // 
            this.buttonCopyDown.Location = new System.Drawing.Point(34, 160);
            this.buttonCopyDown.Name = "buttonCopyDown";
            this.buttonCopyDown.Size = new System.Drawing.Size(118, 23);
            this.buttonCopyDown.TabIndex = 8;
            this.buttonCopyDown.Text = "Copy down";
            this.toolTip1.SetToolTip(this.buttonCopyDown, "Deletes to Recycle Bin");
            this.buttonCopyDown.UseVisualStyleBackColor = true;
            this.buttonCopyDown.Click += new System.EventHandler(this.buttonCopyDown_Click);
            // 
            // buttonSaveManualList
            // 
            this.buttonSaveManualList.Location = new System.Drawing.Point(158, 160);
            this.buttonSaveManualList.Name = "buttonSaveManualList";
            this.buttonSaveManualList.Size = new System.Drawing.Size(118, 23);
            this.buttonSaveManualList.TabIndex = 9;
            this.buttonSaveManualList.Text = "Save";
            this.toolTip1.SetToolTip(this.buttonSaveManualList, "Deletes to Recycle Bin");
            this.buttonSaveManualList.UseVisualStyleBackColor = true;
            this.buttonSaveManualList.Click += new System.EventHandler(this.buttonSaveManualList_Click);
            // 
            // textBoxLog
            // 
            this.textBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLog.Location = new System.Drawing.Point(34, 417);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(613, 152);
            this.textBoxLog.TabIndex = 5;
            // 
            // buttonClearPendingList
            // 
            this.buttonClearPendingList.Location = new System.Drawing.Point(158, 388);
            this.buttonClearPendingList.Name = "buttonClearPendingList";
            this.buttonClearPendingList.Size = new System.Drawing.Size(118, 23);
            this.buttonClearPendingList.TabIndex = 7;
            this.buttonClearPendingList.Text = "Clear";
            this.buttonClearPendingList.UseVisualStyleBackColor = true;
            this.buttonClearPendingList.Click += new System.EventHandler(this.buttonClearPendingList_Click);
            // 
            // buttonCheckPendingList
            // 
            this.buttonCheckPendingList.Location = new System.Drawing.Point(282, 388);
            this.buttonCheckPendingList.Name = "buttonCheckPendingList";
            this.buttonCheckPendingList.Size = new System.Drawing.Size(118, 23);
            this.buttonCheckPendingList.TabIndex = 10;
            this.buttonCheckPendingList.Text = "Check";
            this.buttonCheckPendingList.UseVisualStyleBackColor = true;
            this.buttonCheckPendingList.Click += new System.EventHandler(this.buttonCheckPendingList_Click);
            // 
            // labelManualList
            // 
            this.labelManualList.AutoSize = true;
            this.labelManualList.Location = new System.Drawing.Point(12, 15);
            this.labelManualList.Name = "labelManualList";
            this.labelManualList.Size = new System.Drawing.Size(16, 143);
            this.labelManualList.TabIndex = 11;
            this.labelManualList.Text = "M\nA\nN\nU\nA\nL\n \nL\nI\nS\nT";
            // 
            // labelPendingList
            // 
            this.labelPendingList.AutoSize = true;
            this.labelPendingList.Location = new System.Drawing.Point(12, 192);
            this.labelPendingList.Name = "labelPendingList";
            this.labelPendingList.Size = new System.Drawing.Size(15, 156);
            this.labelPendingList.TabIndex = 12;
            this.labelPendingList.Text = "P\nE\nN\nD\nI\nN\nG\n \nL\nI\nS\nT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 417);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 39);
            this.label1.TabIndex = 13;
            this.label1.Text = "L\nO\nG";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 610);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelPendingList);
            this.Controls.Add(this.labelManualList);
            this.Controls.Add(this.buttonCheckPendingList);
            this.Controls.Add(this.buttonSaveManualList);
            this.Controls.Add(this.buttonCopyDown);
            this.Controls.Add(this.buttonClearPendingList);
            this.Controls.Add(this.buttonClearLog);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.buttonDeleteFiles);
            this.Controls.Add(this.buttonAddNugetCache);
            this.Controls.Add(this.textBoxManualList);
            this.Controls.Add(this.buttonGetNuGetCache);
            this.Controls.Add(this.textBoxPendingList);
            this.Name = "FormMain";
            this.Text = "Clear NuGet directories";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxPendingList;
        private System.Windows.Forms.Button buttonGetNuGetCache;
        private System.Windows.Forms.TextBox textBoxManualList;
        private System.Windows.Forms.Button buttonAddNugetCache;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonDeleteFiles;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Button buttonClearLog;
        private System.Windows.Forms.Button buttonClearPendingList;
        private System.Windows.Forms.Button buttonCopyDown;
        private System.Windows.Forms.Button buttonSaveManualList;
        private System.Windows.Forms.Button buttonCheckPendingList;
        private System.Windows.Forms.Label labelManualList;
        private System.Windows.Forms.Label labelPendingList;
        private System.Windows.Forms.Label label1;
    }
}

