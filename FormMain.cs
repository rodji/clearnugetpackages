﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CleanClean
{
    using System.Configuration;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms.VisualStyles;

    public partial class FormMain : Form
    {
        public FormMain( )
        {
            InitializeComponent( );
            textBoxManualList.Text = LoadManualList();
        }

        private async void buttonGetNuGetCache_Click( object sender, EventArgs e )
        {
            try
            {
                var nuget = NuGetExe.Create();
                await nuget.Start( "locals all -list", ConsoleCallback );
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}

        private void ConsoleCallback(string output, string prefix)
        {
            if (output == null)
                return;

            if (textBoxLog.InvokeRequired)
            {
                this.Invoke( new Action<string, string>( ConsoleCallback ), output, prefix);
            }
            else
            {
                if (prefix != null)
                {
                    textBoxLog.AppendText(prefix + ": ");
                }
                textBoxLog.AppendText(output);
                textBoxLog.AppendText(Environment.NewLine);
            }
        }

        private void AddPath(string path)
        {
            if ( string.IsNullOrEmpty(path) )
                return;
 
            if ( textBoxPendingList.InvokeRequired ) {
                this.Invoke( new Action<string>( AddPath ), path );
            } else
            {
                path = RemoveEndingSlash(path);

                path += "\\*.*";

                textBoxPendingList.AppendText( path );
                textBoxPendingList.AppendText( Environment.NewLine );
            }
        }

        private void buttonClearLog_Click( object sender, EventArgs e )
        {
            textBoxLog.Clear();
        }

        private async void buttonAddNugetCache_Click( object sender, EventArgs e )
        {
            try
            {
                var nuget = NuGetExe.Create();
                NuGetExe.ConsoleOutputAction action = ConsoleCallback;
                action += (line, type) => {
                                              if (type == "stdout" && line != null)
                                              {
                                                  var split = line.Split(new char[]
                                                  {
                                                      ':'
                                                  }, 2);
                                                  if (split.Length == 2)
                                                  {
                                                      AddPath(split[1].Trim());
                                                  }
                                              } }
                    ;

                await nuget.Start( "locals all -list", action );
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonClearPendingList_Click( object sender, EventArgs e )
        {
            textBoxPendingList.Clear();
        }

        private void buttonDeleteFiles_Click( object sender, EventArgs e )
        {
            CheckPendingList();

            if (string.IsNullOrWhiteSpace(textBoxPendingList.Text))
                return;

            if (
                MessageBox.Show(textBoxPendingList.Text, "Are you sure to delete these files?",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;


            var lines = SplitByNewline(textBoxPendingList.Text);
            foreach (var line in lines)
            {
                FileOperationAPIWrapper.MoveToRecycleBin(line);
            }
        }

        private string RemoveEndingSlash(string path)
        {
            if ( path[path.Length - 1] == '\\' ) {
                path = path.Substring( 0, path.Length - 1 );
            }
            return path;
        }

        private string LoadManualList()
        {
            var fullFileName = GetManualFileName();

            return File.Exists( fullFileName ) 
                ? File.ReadAllText(fullFileName, Encoding.UTF8 ) 
                : string.Empty;
        }

        private string GetManualFileName()
        {
            var filePath = ConfigurationManager.AppSettings["ManualListFilePath"] ??
                           Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var fileName = ConfigurationManager.AppSettings["ManualListFileName"] ?? "Manual.txt";

            return RemoveEndingSlash(filePath) + "\\" + fileName;

        }

        private void SaveManualList(string content)
        {
            var fileName = GetManualFileName();
            File.WriteAllText(fileName, content, Encoding.UTF8 );
        }

        private void buttonSaveManualList_Click( object sender, EventArgs e )
        {
            SaveManualList(textBoxManualList.Text);
        }

        private void buttonCopyDown_Click( object sender, EventArgs e )
        {
            var lines = SplitByNewline(textBoxManualList.Text);
            foreach (var line in lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                    continue;
                //if (Directory.Exists(line)) 
                AddPath(line);
            }
        }

        private string[] SplitByNewline(string lines)
        {
            if (lines == null)
                return new string[]{};
            return lines.Split(new string[] {"\r\n", "\n"}, StringSplitOptions.None);
        }

        private void CheckPendingList()
        {
            var lines = SplitByNewline(textBoxPendingList.Text);
            var newListOfLines = new List<string>();
            foreach ( var line in lines ) {
                if ( string.IsNullOrWhiteSpace( line ) )
                    continue;
                var dirname = Path.GetDirectoryName(line);
                if ( dirname == null )
                    continue;
                if ( !Directory.Exists( dirname ) )
                    continue; ;
                newListOfLines.Add( line );
            }
            textBoxPendingList.Text = string.Join( Environment.NewLine, newListOfLines );

        }

        private void buttonCheckPendingList_Click( object sender, EventArgs e )
        {
            CheckPendingList();
        }
    }
}
