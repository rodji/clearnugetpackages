﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanClean
{
    using System.Configuration;
    using System.Diagnostics;
    using System.Dynamic;
    using System.IO;
    using System.Net.Http;
    using System.Reflection;

    public class NuGetExe
    {
        const string NugetExe = "NuGet.exe";

        public string ExePath { get; private set; }

        private NuGetExe()
        {
        }

        private static string Get_NugetExe()
        {
            var exePath = ConfigurationManager.AppSettings["NuGet.exe_Path"] 
                ?? Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + NugetExe;
            if (!File.Exists(exePath))
            {
                Download_NugetExe(exePath);
            }
            return exePath;
        }

        private static void Download_NugetExe( string exePath )
        {
            var http = new HttpClient();
            var response = http.GetAsync(ConfigurationManager.AppSettings["NuGet.exe_URL"]).Result;

            using ( var stream = response.Content.ReadAsStreamAsync( ).Result )
            using ( var fileStream = new FileStream( exePath, FileMode.Create, FileAccess.Write, FileShare.Read ) ) {
                stream.CopyTo( fileStream );
            }
        }

        public static NuGetExe Create()
        {
            return new NuGetExe { ExePath = Get_NugetExe( ) }; ;
        }

        public delegate void ConsoleOutputAction(string line, string type);

        public async Task Start( string arguments, ConsoleOutputAction outputLineCallback )
        {
            CheckExeSign.RequireRightSingature(ExePath);
            var task =  new Task(() =>
            {
                var process = new Process
                {
                    StartInfo =
                    {
                        FileName = NugetExe,
                        Arguments = arguments,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true
                    }
                };

                process.OutputDataReceived += (sender, args) => outputLineCallback(args.Data, "stdout");
                process.ErrorDataReceived += (sender, args) => outputLineCallback(args.Data, "stderr");

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();
            }, TaskCreationOptions.LongRunning);
            task.Start();
            await task;
        }
    }
}
