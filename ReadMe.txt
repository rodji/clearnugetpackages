﻿== What is it? ==
It helps to clear NuGet package cache, packages folders

1. Fill your own directories for clearing in Manual list text box
2. Press 'Copy down'
3. Press Add NuGet cache 
   a) It finds NuGet.exe locally and remotely (tries to download it
   b) Parses output and adds directories in format 'directory\*.*'
4. Press 'Remove files'
   a) It checks and corrects mask-list (pending list)
     - removes empty strings and not existing directories from list
   b) It show confirmation
   c) Moves all files from pending list to Recycle Bin

== Config ==

NuGet.exe_URL        URL where to download from NuGet.exe

NuGet.exe_Path       Path+filename where to find NuGet executable
                       If file isn't exist try to download to this path
                       (optional, default is current executable directory and "NuGet.exe" )

ManualListFileName   Manual file list file name
ManualListFilePath   Manual file list path (optional, default is current executable directory)